pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Veligosha {
    using SafeMath for *;
    //==========================================================================
    // Events
    //==========================================================================
    // Fired when player buy keys
    event BuyKeysEvent (
        address playerAddress,
        uint ethIn,
        uint keysBought
    );

    //==========================================================================
    // Constants
    //==========================================================================
    uint constant private INIT_TIME = 12 hours;
    uint constant private TIME_STEP = 60 seconds;
    uint constant private MAX_TIME = 24 hours;

    //==========================================================================
    // Data
    //==========================================================================
    address private owner = msg.sender;
    bool private activated = false;
    uint private keys = 0;
    uint private timeStart;
    uint private timeEnd;

    //==========================================================================
    // Constructor
    //==========================================================================
    /**
    * Used to make sure no one can interact with contract until
    * it has been activated. 
    */
    modifier isActivated() {
        require(activated, "Contact it's not activated"); 
        _;
    }
    modifier onlyOwner() {
        require(msg.sender == owner, "Sender not authorized.");
        _;
    }

    //==========================================================================
    // Constructor
    //==========================================================================
    constructor() public {}

    //==========================================================================
    // Setup methods
    //==========================================================================
    /**
    * Upon contract deploy, it will be deactivated.
    * This is a one time use function that will activate the contract.
    */
    function activate() public onlyOwner {
        // can run only once
        require(!activated, "already activated");
        // activate the contract 
        activated = true;
        // start round
        timeStart = now;
        timeEnd = timeStart.add(INIT_TIME);
    }

    //==========================================================================
    // Public functions
    //==========================================================================
    function buyKeys() public payable isActivated {
        uint _now = now;
        uint buyPrice = getBuyPrice();
        uint _keys = msg.value.div(buyPrice);
        uint deltaTime = _keys.mul(TIME_STEP);
        keys = keys.add(_keys);
        if (timeEnd < _now) {
            timeEnd = _now.add(deltaTime);
        } else {
            timeEnd = timeEnd.add(deltaTime);
        }
        // Check time max limit
        if (timeEnd.sub(_now) > MAX_TIME) {
            timeEnd = _now.add(MAX_TIME);
        }
        // Fire event
        emit BuyKeysEvent (
            msg.sender,
            msg.value,
            _keys
        );
    }

    //==========================================================================
    // Getters
    //==========================================================================
    /**
    * @dev Calc key price for bought.
    * @return Key price for next bought (wei format)
    */
    function getBuyPrice() public view returns (uint) {
        return 50000000000000;
    }

    /**
     * @dev Return time left.
     * @return time left in seconds
     */
    function getTimeLeft() public view returns (uint) {
        uint _now = now;
        uint result = 0;

        if (_now < timeEnd)
            result = timeEnd.sub(_now);

        return result;
    }

    /**
     * @dev Return end time.
     * @return end time
     */
    function getTimeEnd() public view returns (uint) {
        return timeEnd;
    }

    /**
    * @dev Return total keys.
    * @return total keys
    */
    function getTotalKeys() public view returns (uint) {
        return keys;
    }

    /**
    * @dev Return total info.
    * @return time left, time end, buy price, total keys
    */
    function getTotalInfo()
        public
        view
        returns (uint, uint, uint, uint)
    {
        uint timeLeft = getTimeLeft();
        uint buyPrice = getBuyPrice();

        return (timeLeft, timeEnd, buyPrice, keys);
    }
}
